<?php

class MyLogger
{
    public $pathToLogFile;

    function log($message)
    {
        if ($this->checkPathToLogFile()) {
            file_put_contents($this->pathToLogFile, $this->formatMessage($message), FILE_APPEND);
        }
    }

    function checkPathToLogFile()
    {
        if (file_exists($this->pathToLogFile)) {
            return true;
        } else {
            return false;
        }
    }

    function formatMessage($message)
    {
        $dateTime = new DateTime();
        return "{$dateTime->format(DateTime::ISO8601)}: {$message}" . PHP_EOL;
    }
}
